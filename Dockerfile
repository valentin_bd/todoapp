FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

RUN dotnet --version

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY Todo.API/*.csproj ./Todo.API/
COPY Todo.Domain/*.csproj ./Todo.Domain/
COPY Todo.Infra/*.csproj ./Todo.Infra/

RUN dotnet restore "Todo.API/Todo.API.csproj"

# Copy everything else and build
COPY Todo.API/. ./Todo.API/
COPY Todo.Domain/. ./Todo.Domain/
COPY Todo.Infra/. ./Todo.Infra/

WORKDIR /app
RUN dotnet publish Todo.API/Todo.API.csproj -c Release -o ./Todo.API/out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app

# Maj de la Timezone pour Paris
RUN ln -snf /usr/share/zoneinfo/Europe/Paris /etc/localtime && echo Europe/Paris > /etc/timezon

COPY --from=build-env /app/Todo.API/out .
ENV ASPNETCORE_URLS=http://+:5000
ENTRYPOINT ["dotnet", "Todo.API.dll"]
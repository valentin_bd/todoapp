namespace Todo.Domain.Models
{
    public class TaskItem
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public bool Achieved { get; set; }
        public User Owner { get; set; }
        public int OwnerId { get; set; }
    }
}
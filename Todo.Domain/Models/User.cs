using System.Collections.Generic;

namespace Todo.Domain.Models
{
    public class User
    {
        public User()
        {
            Tasks = new List<TaskItem>();
        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public ICollection<TaskItem> Tasks { get; set; }
    }
}
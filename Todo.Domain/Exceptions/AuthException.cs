using System;

namespace Todo.Domain.Exceptions
{
    public class AuthException : Exception
    {
        public AuthException() : base("Authentication failed")
        {

        }
    }
}
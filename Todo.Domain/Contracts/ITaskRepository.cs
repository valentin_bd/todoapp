using System.Collections.Generic;
using System.Threading.Tasks;
using Todo.Domain.Models;

namespace Todo.Domain.Contracts
{
    public interface ITaskRepository
    {
        Task<ICollection<TaskItem>> GetAllTaskAsync();
        Task<ICollection<TaskItem>> GetUserTasksAsync(int userId);
        Task<TaskItem> AddTaskAsync(TaskItem task);
    }
}
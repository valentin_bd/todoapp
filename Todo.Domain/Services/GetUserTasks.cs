using System.Collections.Generic;
using System.Threading.Tasks;
using Todo.Domain.Contracts;
using Todo.Domain.Models;

namespace Todo.Domain.Services
{
    public class GetUserTasks
    {
        private readonly ITaskRepository _taskRepository;

        public GetUserTasks(ITaskRepository taskRepo)
        {
            this._taskRepository = taskRepo;
        }

        public async Task<ICollection<TaskItem>> GetTasks(int userId)
        {
            return await _taskRepository.GetUserTasksAsync(userId);
        }
    }
}
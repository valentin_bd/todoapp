using System.Threading.Tasks;
using Todo.Domain.Contracts;
using Todo.Domain.Models;

namespace Todo.Domain.Services
{
    public class CreateTaskItem
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;

        public CreateTaskItem(ITaskRepository taskRepo, IUserRepository userRepo)
        {
            this._taskRepository = taskRepo;
            this._userRepository = userRepo;
        }

        public async Task<TaskItem> CreateAsync(int userId, string label, bool achieved)
        {
            var user = await _userRepository.GetUserByIdAsync(userId);
            var newTask = new TaskItem
            {
                Owner = user,
                Label = label,
                Achieved = achieved
            };
            return await _taskRepository.AddTaskAsync(newTask);
        }
    }
}
using System.Threading.Tasks;
using Todo.Domain.Contracts;
using Todo.Domain.Models;

namespace Todo.Domain.Services
{
    public class AuthenticateUser
    {
        private readonly IUserRepository _userRepository;

        public AuthenticateUser(IUserRepository userRepo)
        {
            this._userRepository = userRepo;
        }

        public async Task<User> Login(string username, string password)
        {
            return await _userRepository.AuthenticateUserAsync(username, password);
        }
    }
}
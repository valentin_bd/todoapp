using System.ComponentModel.DataAnnotations;

namespace Todo.API.ViewModels
{
    public class CreateTaskItemViewModel
    {
        [Required]
        public string label { get; set; }
        public bool achieved { get; set; }
    }
}
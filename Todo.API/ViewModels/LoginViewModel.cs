using System.ComponentModel.DataAnnotations;

namespace Todo.API.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
    }
}
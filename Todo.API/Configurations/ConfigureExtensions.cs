using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Todo.API.Helpers;
using Todo.Domain.Contracts;
using Todo.Domain.Services;
using Todo.Infra.Contexts;
using Todo.Infra.Repositories;

namespace Todo.API.Configurations
{
    public static class ConfigureExtensions
    {

        public static void AddTodoContext(this IServiceCollection services)
        {
            services.AddDbContext<TodoAppContext>(options =>
            {
                options.UseInMemoryDatabase("todo-app");
            });
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ITaskRepository, TaskRepository>();
        }

        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddTransient<CreateTaskItem>();
            services.AddTransient<GetUserTasks>();
            services.AddTransient<AuthenticateUser>();
        }

        public static void AddAPIServices(this IServiceCollection services)
        {
            services.AddTransient<TokenGenerator>();
        }

        public static void AddEnvSettings(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<AppSettings>(config.GetSection("AppSettings"));
        }

        public static void AddConfigAuthentication(this IServiceCollection services, string secret)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret))
                    };
                });
        }

    }
}
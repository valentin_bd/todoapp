using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Todo.API.ViewModels;
using Todo.Domain.Services;

namespace Todo.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class TaskItemController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> Index([FromServices] GetUserTasks handler)
        {
            var identity = User.Identity as ClaimsIdentity;
            var userId = identity.Claims.Where(claim => claim.Type == "id").FirstOrDefault().Value;
            var tasks = await handler.GetTasks(int.Parse(userId));
            return Ok(tasks);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateTaskItemViewModel model, [FromServices] CreateTaskItem handler)
        {
            var identity = User.Identity as ClaimsIdentity;
            var userId = identity.Claims.Where(claim => claim.Type == "id").FirstOrDefault().Value;
            var newTask = await handler.CreateAsync(int.Parse(userId), model.label, model.achieved);
            return Ok(new
            {
                id = newTask.Id,
                label = newTask.Label,
                achieved = newTask.Achieved
            });
        }

    }
}
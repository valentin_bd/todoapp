using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Todo.API.Helpers;
using Todo.API.ViewModels;
using Todo.Domain.Services;

namespace Todo.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {

        [HttpPost]
        public async Task<IActionResult> Login(
            LoginViewModel model,
            [FromServices] AuthenticateUser handler,
            [FromServices] TokenGenerator generator
        )
        {
            try
            {
                var authUser = await handler.Login(model.username, model.password);
                var token = generator.GenerateJWT(authUser);
                return Ok(new { access_token = token });
            }
            catch (System.Exception)
            {
                return BadRequest(new { message = "Authentication failed" });
            }
        }

    }
}
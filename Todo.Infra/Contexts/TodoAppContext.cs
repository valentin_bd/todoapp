using Microsoft.EntityFrameworkCore;
using Todo.Domain.Models;

namespace Todo.Infra.Contexts
{
    public class TodoAppContext : DbContext
    {
        public TodoAppContext(DbContextOptions<TodoAppContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<TaskItem> TaskItems { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // set PK
            modelBuilder.Entity<User>().HasKey(t => t.Id);
            modelBuilder.Entity<TaskItem>().HasKey(t => t.Id);

            // set the relationship between task and owner
            modelBuilder.Entity<TaskItem>()
                    .HasOne(t => t.Owner)
                    .WithMany(u => u.Tasks);


        }

    }
}
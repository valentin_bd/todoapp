using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Todo.Domain.Contracts;
using Todo.Domain.Exceptions;
using Todo.Domain.Models;
using Todo.Infra.Contexts;

namespace Todo.Infra.Repositories
{
    public class TaskRepository : ITaskRepository
    {

        private readonly TodoAppContext _context;

        public TaskRepository(TodoAppContext context)
        {
            this._context = context;
        }

        public async Task<TaskItem> AddTaskAsync(TaskItem task)
        {
            _context.TaskItems.Add(task);
            await _context.SaveChangesAsync();
            return task;
        }

        public async Task<ICollection<TaskItem>> GetAllTaskAsync()
        {
            return await _context.TaskItems.ToListAsync();
        }

        public async Task<ICollection<TaskItem>> GetUserTasksAsync(int userId)
        {
            return await _context.TaskItems.Where(task => task.OwnerId == userId).ToListAsync();
        }
    }
}
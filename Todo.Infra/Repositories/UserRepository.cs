using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Todo.Domain.Contracts;
using Todo.Domain.Exceptions;
using Todo.Domain.Models;
using Todo.Infra.Contexts;

namespace Todo.Infra.Repositories
{
    public class UserRepository : IUserRepository
    {

        private readonly TodoAppContext _context;

        public UserRepository(TodoAppContext context)
        {
            this._context = context;
        }
        public async Task<User> GetUserByIdAsync(int id)
        {
            var user = await _context.Users.Where(user => user.Id == id).FirstOrDefaultAsync();
            if (user == null) { throw new ModelNotFoundException("User not found"); }
            return user;
        }

        public async Task<ICollection<User>> GetUsersAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> AuthenticateUserAsync(string username, string password)
        {
            var user = await _context.Users.Where(user =>
                user.Username == username && user.Password == password
            ).FirstOrDefaultAsync();
            if (user == null) { throw new AuthException(); }
            return user;
        }

    }
}
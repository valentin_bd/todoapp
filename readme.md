# Execution

## Via docker

A la racine de la solution :

1 - Build de l'image

`docker build -f "Dockerfile" -t todoapp:latest .`

2 - Lancer un conteneur mapé sur le port 5000 (redirection https désactivée)

`docker run -p 5000:5000 todoapp`

## Via dotnet

A la racine de la solution :

`dotnet restore`
`dotnet run`